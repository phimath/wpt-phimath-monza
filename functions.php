<?php
/**
 * Monza functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Monza
 */

if ( ! function_exists( 'monza_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function monza_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Monza, use a find and replace
		 * to change 'monza' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'monza', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
            array(
                'primary' => esc_html__('Primary Menu', 'monza'),
                'social' => esc_html__('Social Menu', 'monza')
            )
        );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'monza_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'monza_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function monza_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'monza_content_width', 640 );
}
add_action( 'after_setup_theme', 'monza_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function monza_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'monza' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'monza' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'monza_widgets_init' );

/**
 * Enqueue Google fonts
 */
function monza_enqueue_googlefonts()
{
    $fonts_url = '';
    $roboto = _x( 'on', 'Roboto font: on or off', 'monza' );
    
    if( 'off' != $roboto )
    {
        $font_families = array();
        if ( 'off' !== $roboto ) $font_families[] = 'Roboto:300,400';
        $query_args = array('family' => urlencode(implode('|', $font_families)), 'subset' => urlencode('latin,latin-ext'));
        $fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
    }

    wp_enqueue_style('monza-googlefonts', esc_url_raw($fonts_url), array(), null);
}
add_action( 'wp_enqueue_scripts', 'monza_enqueue_googlefonts' );

/**
 * Enqueue scripts and styles.
 */
function monza_scripts() {
	
	wp_enqueue_script( 'monza-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	wp_enqueue_script( 'monza-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );    
    wp_enqueue_style('bootstrap', get_template_directory_uri() . '/libs/bootstrap/css/bootstrap.min.css');
    wp_enqueue_style('font-awesome', get_template_directory_uri() . '/libs/font-awesome/css/font-awesome.min.css');
    wp_enqueue_style('owl-carousel', get_template_directory_uri() . '/libs/owl/owl.carousel.css');
    wp_enqueue_style( 'monza-style', get_stylesheet_uri() );
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
    
    wp_enqueue_script('owl-carousel', get_template_directory_uri() . '/libs/owl/owl.carousel.min.js', array(), false, true);
    wp_enqueue_script('monza-scripts', get_template_directory_uri() . '/js/monza-script.js', array('jquery'), '', true );

    if ( function_exists('monza_custom_style')) {
        wp_add_inline_style( 'monza-style', monza_custom_style() );
    }

    // Custom icons
    $icons = [
        'facebook',
        'xing',
        'github',
        'gitlab',
        'twitter',
        'instagram',
        'linkedin'
    ];

    // If it exists, include it.
    $icons_css = '';
    foreach ($icons as $icon ){
        $physical = get_parent_theme_file_path('/assets/img/icon-' . $icon . '.png');
        if ( file_exists( $physical ) ) {
            $logical = get_template_directory_uri() . '/assets/img/icon-' . $icon . '.png';
            $icons_css .= '.icon-' . $icon . ' > a { 
                background: url(' . $logical .') no-repeat center; 
                background-size: contain; 
            }';
        }
    }
    wp_add_inline_style('monza-style', $icons_css);
}

add_action( 'wp_enqueue_scripts', 'monza_scripts' );

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

