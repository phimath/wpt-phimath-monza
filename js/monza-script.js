(function($){
		
    $(document).ready(function() {
        if ( $('.featured-area .slider').length )
        {
            $('.featured-area .slider').owlCarousel({
        		items:1,
                nav:false,
                dots:true,
                autoplay: true,
                loop: true,
                smartSpeed: 1000
        	});
        }
    });
})(jQuery);
