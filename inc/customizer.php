<?php
function monza_sanitize_checkbox($checked)
{
    return ((isset($checked) && true == $checked) ? true : false);
}

function monza_sanitize_number_absint($number, $setting)
{
    // Ensure $number is an absolute integer (whole number, zero or greater).
    $number = absint($number);

    // If the input is an absolute integer, return it; otherwise, return the default
    return ($number ? $number : $setting->default);
}

function monza_sanitize_select($input, $setting)
{

    // Ensure input is a slug.
    $input = sanitize_key($input);

    // Get list of choices from the control associated with the setting.
    $choices = $setting->manager->get_control($setting->id)->choices;

    // If the input is a valid key, return it; otherwise, return the default.
    return (array_key_exists($input, $choices) ? $input : $setting->default);
}

if (class_exists('WP_Customize_Control')) {
    class WP_Customize_Category_Control extends WP_Customize_Control
    {
        /**
         * Render the control's content.
         *
         * @since 3.4.0
         */
        public function render_content()
        {
            $dropdown = wp_dropdown_categories(
                array(
                    'name' => '_customize-dropdown-categories-' . $this->id,
                    'echo' => 0,
                    'show_option_none' => esc_html__('&mdash; Select &mdash;', 'monza'),
                    'option_none_value' => '0',
                    'selected' => $this->value(),
                )
            );

            // Hackily add in the data link parameter.
            $dropdown = str_replace('<select', '<select ' . $this->get_link(), $dropdown);

            printf(
                '<label class="customize-control-select"><span class="customize-control-title">%s</span> %s</label>',
                $this->label,
                $dropdown
            );
        }
    }
}

/**
 * Monza Theme Customizer
 *
 * @package Monza
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function monza_customize_register($wp_customize)
{
    $wp_customize->get_setting('blogname')->transport = 'postMessage';
    $wp_customize->get_setting('blogdescription')->transport = 'postMessage';

    if (isset($wp_customize->selective_refresh)) {
        $wp_customize->selective_refresh->add_partial('blogname', array(
            'selector' => '.site-title a',
            'render_callback' => 'monza_customize_partial_blogname',
        ));
        $wp_customize->selective_refresh->add_partial('blogdescription', array(
            'selector' => '.site-description',
            'render_callback' => 'monza_customize_partial_blogdescription',
        ));
    }

    $wp_customize->add_panel('monza-theme-options-panel', array(
        'priority' => 2,
        'capability' => 'edit_theme_options',
        'title' => esc_html__('Monza: Theme Options', 'monza')
    ));

    /** Featured Posts Slider */
    $wp_customize->add_section('monza_featured_posts', array('title' => esc_html__('Featured Posts', 'monza'), 'panel' => 'monza-theme-options-panel'));

    $wp_customize->add_setting('monza_featured_posts_enable', array('default' => false, 'sanitize_callback' => 'monza_sanitize_checkbox'));
    $wp_customize->add_setting('monza_featured_posts_catid', array('default' => '', 'sanitize_callback' => 'sanitize_text_field'));
    $wp_customize->add_setting('monza_featured_posts_ids', array('default' => '', 'sanitize_callback' => 'sanitize_text_field'));
    $wp_customize->add_setting('monza_featured_posts_showposts', array('default' => 5, 'sanitize_callback' => 'monza_sanitize_number_absint'));

    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'monza_featured_posts_enable',
            array(
                'label' => esc_html__('Enable featured posts?', 'monza'),
                'section' => 'monza_featured_posts',
                'type' => 'checkbox'
            )
        )
    );

    $wp_customize->add_control(
        new WP_Customize_Category_Control(
            $wp_customize,
            'monza_featured_posts_catid',
            array(
                'label' => esc_html__('Select Category', 'monza'),
                'section' => 'monza_featured_posts',
                'type' => 'select'
            )
        )
    );

    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'monza_featured_posts_ids',
            array(
                'label' => esc_html__('Or input post/page IDs', 'monza'),
                'section' => 'monza_featured_posts',
                'type' => 'text',
                'description' => esc_html__('E.g. 5,6,8. Note: Separate each ID with a comma. The IDs you put in here will override the featured category option above.', 'monza')
            )
        )
    );

    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'monza_featured_posts_showposts',
            array(
                'label' => esc_html__('Posts per page', 'monza'),
                'section' => 'monza_featured_posts',
                'type' => 'text',
                'description' => esc_html__('Number of posts to show', 'monza')
            )
        )
    );

    /** Post Excerpt */
    $wp_customize->add_section('post_exerpt', array('title' => esc_html__('Post Excerpt', 'monza'), 'panel' => 'monza-theme-options-panel'));

    $wp_customize->add_setting('monza_accent_color', array('default' => '#ad8353', 'sanitize_callback' => 'sanitize_hex_color'));
    $wp_customize->add_setting('monza_custom_excerpt', array('default' => '', 'sanitize_callback' => 'monza_sanitize_checkbox'));
    $wp_customize->add_setting('monza_custom_excerpt_length', array('default' => '23', 'sanitize_callback' => 'monza_sanitize_number_absint'));

    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'monza_custom_excerpt',
            array(
                'label' => esc_html__('Custom excerpt?', 'monza'),
                'section' => 'post_exerpt',
                'type' => 'checkbox'
            )
        )
    );

    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'monza_custom_excerpt_length',
            array(
                'label' => esc_html__('Excerpt length', 'monza'),
                'section' => 'post_exerpt',
                'type' => 'number'
            )
        )
    );

    /** Footer */
    $wp_customize->add_section('monza_footer', array('title' => esc_html__('Footer', 'monza'), 'panel' => 'monza-theme-options-panel'));

    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'monza_custom_footer',
            array(
                'label' => esc_html__('Footer custom HTML', 'monza'),
                'section' => 'monza_footer',
                'type' => 'text'
            )
        )
    );
    /** Accent color */
    $wp_customize->add_section('accent_color', array('title' => esc_html__('Accent Color', 'monza'), 'panel' => 'monza-theme-options-panel'));

    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'monza_accent_color',
            array(
                'label' => esc_html__('Accent Color', 'monza'),
                'section' => 'accent_color'
            )
        )
    );
}

add_action('customize_register', 'monza_customize_register');

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function monza_customize_partial_blogname()
{
    bloginfo('name');
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function monza_customize_partial_blogdescription()
{
    bloginfo('description');
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function monza_customize_preview_js()
{
    wp_enqueue_script('monza-customizer', get_template_directory_uri() . '/js/customizer.js', array('customize-preview'), '20151215', true);
}

add_action('customize_preview_init', 'monza_customize_preview_js');
