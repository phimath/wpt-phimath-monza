<?php
    if ( in_the_loop() ) {
		$thumb_id     = get_post_thumbnail_id();
		$facebook     = add_query_arg( 'u', get_permalink(), 'https://www.facebook.com/sharer.php' );
		$twitter      = add_query_arg( 'url', get_permalink(), 'https://twitter.com/share' );
		$gplus        = add_query_arg( 'url', get_permalink(),	'https://plus.google.com/share' );
	?>
		
	<a href="<?php echo esc_url( $facebook ); ?>" target="_blank" class="social-icon"><i class="fa fa-facebook"></i></a>
	<a href="<?php echo esc_url( $twitter ); ?>" target="_blank" class="social-icon"><i class="fa fa-twitter"></i></a>
	<a href="<?php echo esc_url( $gplus ); ?>" target="_blank" class="social-icon"><i class="fa fa-google-plus"></i></a>
    <?php if ( !empty( $thumb_id ) ) {
		$pinterest = add_query_arg(
            array(
                'url' => get_permalink(),
                'description' => get_the_title(),
                'media' => monza_get_image_src( get_post_thumbnail_id(), 'large' ),
			), 'https://pinterest.com/pin/create/bookmarklet/'
		);
	?>
	<a href="<?php echo esc_url( $pinterest ); ?>" target="_blank" class="social-icon"><i class="fa fa-pinterest"></i></a>
    <?php } ?>

<?php } ?>