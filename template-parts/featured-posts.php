<?php
$catid      = get_theme_mod('monza_featured_posts_catid');
$ids        = get_theme_mod('monza_featured_posts_ids');
$showposts  = get_theme_mod('monza_featured_posts_showposts');

if ( $ids ) {
	$featured_posts = explode(',', $ids);
	$args = array( 'ignore_sticky_posts' => 1, 'showposts' => $showposts, 'post_type' => array('post', 'page'), 'post__in' => $featured_posts, 'orderby' => 'post__in' );
} else {
	$args = array( 'cat' => (int)$catid, 'showposts' => $showposts );
}
$featured_posts_query = new WP_Query( $args );
if ( $featured_posts_query->have_posts() ) { ?>
<div class="featured-area">
   <div class="slider">
        <?php while ( $featured_posts_query->have_posts() ) { ?>
            <?php
                $featured_posts_query->the_post();
                $image_featured = monza_resize_image( get_post_thumbnail_id() , wp_get_attachment_thumb_url(), 1170, 600, true, true );
                $image_featured = $image_featured['url'];
            ?>
        <div class="slide-item post" style="background-image: url(<?php echo esc_url($image_featured); ?>);">
            <div class="slide-item-text">
    			<div class="post-text-inner">
                    <div class="entry-cat"><?php the_category(', '); ?></div>
        			<h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                </div>
    		</div>
        </div>		
        <?php } ?>
    </div>
</div>
<?php } ?>
<?php wp_reset_postdata(); ?>